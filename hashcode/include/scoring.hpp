// Copyright 2019 ReSnap b.v. All Rights Reserved.

#ifndef SCORING_HPP
#define SCORING_HPP

#include <set>

#include "photo.hpp"
#include "slide.hpp"

std::set<std::string> diff(const Slide &lhs, const Slide &rhs);

int trans_score(const Slide &lhs, const Slide &rhs);

int score(const std::vector<Slide> &slides);

#endif // SCORING_HPP
