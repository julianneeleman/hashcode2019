// Copyright 2019 ReSnap b.v. All Rights Reserved.

#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <random>

template <typename InputIt> InputIt random_sample(InputIt first, InputIt last) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::uniform_int_distribution<int> choose(0, std::distance(first, last));
    std::advance(first, choose(gen));
    return first;
}

#endif // RANDOM_HPP
