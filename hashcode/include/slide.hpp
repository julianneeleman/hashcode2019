// Copyright 2019 ReSnap b.v. All Rights Reserved.

#ifndef SLIDE_HPP
#define SLIDE_HPP

#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>
#include <vector>

#include "photo.hpp"

class Slide {
  public:
    explicit Slide(Photo photo);
    Slide(Photo lhs, Photo rhs);

    const std::vector<int> &indices() const;
    const std::set<std::string> &tags() const;

  private:
    std::vector<int> m_indices;
    std::set<std::string> m_tags;
};

std::ostream &operator<<(std::ostream &os, const Slide &slide);

void print_slides(const std::vector<Slide> &slides);

#endif // SLIDE_HPP
