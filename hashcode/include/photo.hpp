// Copyright 2019 ReSnap b.v. All Rights Reserved.

#ifndef PHOTO_HPP
#define PHOTO_HPP

#include <string>
#include <set>

struct Photo {
    bool hor;
    int index;
    std::set<std::string> tags;
};

#endif // PHOTO_HPP
