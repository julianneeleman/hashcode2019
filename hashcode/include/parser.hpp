// Copyright 2019 ReSnap b.v. All Rights Reserved.

#ifndef PARSER_HPP
#define PARSER_HPP

#include <iostream>
#include <set>
#include <vector>

#include "photo.hpp"

std::vector<Photo> parse_input();


#endif // PARSER_HPP
