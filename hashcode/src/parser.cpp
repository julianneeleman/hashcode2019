// Copyright 2019 ReSnap b.v. All Rights Reserved.

#include "parser.hpp"

std::vector<Photo> parse_input() {
    int n;
    std::cin >> n;
    std::vector<Photo> photos(n);
    for (int i = 0; i < n; i++) {
        photos[i].index = i;
        int m;
        char orient;
        std::cin >> orient;
        photos[i].hor = orient == 'H';
        std::cin >> m;
        for (int j = 0; j < m; j++) {
            std::string tag;
            std::cin >> tag;
            photos[i].tags.insert(tag);
        }
    }
    return photos;
}
