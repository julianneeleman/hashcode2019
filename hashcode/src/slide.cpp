// Copyright 2019 ReSnap b.v. All Rights Reserved.

#include "slide.hpp"

Slide::Slide(Photo photo) : m_indices({photo.index}), m_tags(photo.tags) {
    assert(photo.hor);
}

Slide::Slide(Photo lhs, Photo rhs)
    : m_indices({lhs.index, rhs.index}), m_tags(lhs.tags) {
    assert(!lhs.hor);
    assert(!rhs.hor);
    for (const std::string &s : rhs.tags) {
        m_tags.insert(s);
    }
}

const std::vector<int> &Slide::indices() const { return m_indices; }

const std::set<std::string> &Slide::tags() const { return m_tags; }

std::ostream &operator<<(std::ostream &os, const Slide &slide) {
    for (int index : slide.indices()) {
        os << index << " ";
    }
    return os;
}

void print_slides(const std::vector<Slide> &slides) {
    std::cout << slides.size() << std::endl;
    for (const Slide &t : slides) {
        std::cout << t << std::endl;
    }
}
