// Copyright 2019 ReSnap b.v. All Rights Reserved.

#include "scoring.hpp"

std::set<std::string> diff(const Slide &lhs, const Slide &rhs) {
    std::set<std::string> d;
    std::set_difference(lhs.tags().begin(), lhs.tags().end(),
                        rhs.tags().begin(), rhs.tags().end(),
                        std::insert_iterator<std::set<std::string>>(d, d.end()));
    return d;
}

int trans_score(const Slide &lhs, const Slide &rhs) {
    int anb = diff(lhs, rhs).size(), bna = diff(rhs, lhs).size(),
        intersect = lhs.tags().size() - anb;
    return std::min(std::min(anb, bna), intersect);
}

int score(const std::vector<Slide> &slides) {
    int score = 0;
    for (int i = 0; i < slides.size() - 1; i++) {
        score += trans_score(slides[i], slides[i + 1]);
    }
    return score;
}
