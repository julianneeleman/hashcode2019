// Copyright 2019 ReSnap b.v. All Rights Reserved.

#include <list>

#include "parser.hpp"
#include "photo.hpp"
#include "random.hpp"
#include "scoring.hpp"

bool sort_by_tag_size(const Photo &lhs, const Photo &rhs) {
    return lhs.tags < rhs.tags;
}

std::vector<Slide> greedy(std::vector<Photo> photos) {
    std::vector<Slide> ans;
    bool v = false;
    Photo t;
    for (const Photo &p : photos) {
        if (p.hor) {
            ans.emplace_back(p);
        } else if (v) {
            ans.emplace_back(t, p);
            v = false;
        } else {
            v = true;
            t = p;
        }
    }
    return ans;
}

std::vector<Slide> sort_solution(std::vector<Photo> photos) {
    std::sort(photos.begin(), photos.end(), sort_by_tag_size);
    return greedy(photos);
}

void add_slide(std::list<Slide> &slides, Slide slide) {
    int best = 0;
    auto index = slides.begin();
    for (auto it = slides.begin(); it != std::prev(slides.end()); it++) {
        Slide a = *it, b = *std::next(it);
        int total =
            trans_score(a, slide) + trans_score(slide, b) - trans_score(a, b);
        if (total > best) {
            best = total;
            index = std::next(it);
        }
    }
    slides.insert(index, slide);
}

std::vector<Slide> insertion_solution(std::vector<Photo> photos) {
    std::list<Slide> ans;
    bool v = false;
    Photo t;
    for (int i = 0; i < photos.size(); i++) {
        if (i % 100 == 0) {
            std::clog << i << std::endl;
        }
        const Photo &p = photos[i];
        if (p.hor) {
            add_slide(ans, Slide(p));
        } else if (v) {
            add_slide(ans, Slide(t, p));
            v = false;
        } else {
            v = true;
            t = p;
        }
    }
    return greedy(photos);
}

bool is_single_tag_hor(const Photo &photo) {
    return photo.tags.size() <= 1 && photo.hor;
}

void filter(std::vector<Photo> &photos) {
    photos.erase(
        std::remove_if(photos.begin(), photos.end(), is_single_tag_hor),
        photos.end());
}

void bubble_search_ofzo(std::vector<Slide> &slides, int its) {
    if (slides.size() < 4) {
        return;
    }
    for (int i = 0; i < its; i++) {
        auto a = random_sample(slides.begin() + 1, slides.end() - 2),
             b = random_sample(slides.begin() + 1, slides.end() - 2);
        int add = trans_score(*(a - 1), *b) + trans_score(*b, *(a + 1)) +
                  trans_score(*(b - 1), *a) + trans_score(*a, *(b + 1)),
            sub = trans_score(*(a - 1), *a) + trans_score(*a, *(a + 1)) +
                  trans_score(*(b - 1), *b) + trans_score(*b, *(b + 1));
        if (add > sub || rand() % its < i) {
            std::iter_swap(a, b);
        }
        if (i % 100 == 0) {
            std::clog << score(slides) << std::endl;
        }
    }
}

int main() {
    std::vector<Photo> photos = parse_input();
    auto slides = sort_solution(photos);
    bubble_search_ofzo(slides, 10);
    print_slides(slides);
    std::cerr << score(slides) << std::endl;
    return 0;
}
